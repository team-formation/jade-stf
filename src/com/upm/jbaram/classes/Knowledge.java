package com.upm.jbaram.classes;

import org.javatuples.Quartet;
import java.util.*;


public final class Knowledge {

    public static String FILENAME = "data/stf_10_5-8min.csv";
    public static Integer TF_K = 1;
    public static Integer TF_ITER = 0;

    public static int BOUNDING_FUNCTION = 0;

    // AGENT, DEPLOYMENT COST, RECOVERY COST, AFFORDABLE TASKS
    public static final List<Quartet<Integer, Integer, Integer, List<Integer>>> TF_agents = new ArrayList<>();
    public static final Set<Integer> TF_goal = new TreeSet<>();
    public static final List<Quartet<Integer, Integer, Map<Integer, Integer>,Map<Integer, Integer>>> solution = new ArrayList<>();

    private static Map<Integer, Integer> teamCache = new HashMap<>();
    private static Integer costly = Integer.MAX_VALUE;

    private static void clear() {
        costly = Integer.MAX_VALUE;
        TF_agents.clear();
        TF_goal.clear();
        solution.clear();
        teamCache = new HashMap<>();
    }

    public static void pushSolution (int costly, Map<Integer, Integer> recoToken) {
        if (solution.stream().anyMatch(v -> costly < v.getValue0() + v.getValue1())) solution.clear();

        int[] costs = new int[2];
        getTeamCache().forEach((k, v) -> costs[0] += v==1 ? TF_agents.stream().filter(a -> a.getValue0().equals(k)).findFirst().get().getValue1() : 0);
        recoToken.forEach((k,v) -> costs[1] += v==1 ? TF_agents.stream().filter(a -> a.getValue0().equals(k)).findFirst().get().getValue2() : 0);
        solution.add(new Quartet<>(costs[0], costs[1], getTeamCache(), recoToken));
    }

    // Careful with overflow costs.
    /**
     * From paper example
     * @param k value for k-stabilizability
     */
    public static boolean loadTF1(int k) {

        TF_goal.addAll(Set.of(1, 2));
        TF_K = k;

        TF_agents.add(new Quartet<>(1, 5, 2, List.of(1, 2, 3)));
        TF_agents.add(new Quartet<>(2, 3, 1, List.of(2, 3)));
        TF_agents.add(new Quartet<>(3, 2, 1, List.of(3)));
        TF_agents.add(new Quartet<>(4, 1, 1, List.of(1)));
        TF_agents.add(new Quartet<>(5, 2, 3, List.of(2)));
        TF_agents.add(new Quartet<>(6, 1, 2, List.of(1, 3)));

        return true;
    }

    /**
     * From Okimoto et al.
     * @param k value for k-stabilizability
     */
    public static boolean loadTF2(int k) {

        TF_goal.addAll(Set.of(1, 3));
        TF_K = k;

        TF_agents.add(new Quartet<>(1, 4, 20, List.of(1, 2)));
        TF_agents.add(new Quartet<>(2, 3, 20, List.of(1, 3)));
        TF_agents.add(new Quartet<>(3, 5, 20, List.of(1, 2, 3)));
        TF_agents.add(new Quartet<>(4, 2, 20, List.of(3, 4)));
        TF_agents.add(new Quartet<>(5, 9, 20, List.of(1, 2, 4, 5)));
        TF_agents.add(new Quartet<>(6, 1, 20, List.of(5)));

        return true;
    }

    /**
     * From Demirovic et al.
     * @param k value for k-stabilizability
     */
    public static boolean loadTF3(int k) {

        TF_goal.addAll(Set.of(11, 12, 13, 21, 22, 23, 31, 32, 33));
        TF_K = k;

        TF_agents.add(new Quartet<>(11, 20, 10, List.of(11, 12, 21)));
        TF_agents.add(new Quartet<>(12, 20, 10, List.of(11, 12, 13, 22)));
        TF_agents.add(new Quartet<>(13, 20, 10, List.of(12, 13, 23)));
        TF_agents.add(new Quartet<>(21, 20, 10, List.of(11, 21, 22, 31)));
        TF_agents.add(new Quartet<>(22, 80, 1000, List.of(11, 12, 13, 21, 22, 23, 31, 32, 33)));
        TF_agents.add(new Quartet<>(23, 20, 10, List.of(13, 22, 23, 33)));
        TF_agents.add(new Quartet<>(31, 20, 10, List.of(21, 31, 32)));
        TF_agents.add(new Quartet<>(32, 20, 10, List.of(22, 31, 32, 33)));
        TF_agents.add(new Quartet<>(33, 20, 10, List.of(23, 32, 33)));

        return true;
    }

    public static Map<Integer, Integer> getTeamCache() {
        return teamCache;
    }

    public static void setTeamCache(Map<Integer, Integer> teamCache) { Knowledge.teamCache = teamCache; }

    public static Integer getTfK() { return TF_K; }

    public static void genTFProblem(String[] data) {
        clear();
        int agentIndex = 1;
        for (String agent : data) {
            int dCost = 1+new Random().nextInt(100), hCost = dCost;
            Set<Integer> skills = new HashSet<>();
            for (Integer skill : Arrays.stream(agent.split(" ")).mapToInt(Integer::parseInt).toArray())
                skills.add(skill);
            TF_goal.addAll(skills);
            TF_agents.add(new Quartet<>(agentIndex++, dCost, hCost, List.copyOf(skills)));
        }
        Stabilizability.genDelta(agentIndex-1);
        System.out.println(TF_goal); // DEBUGGING
        //System.out.println(TF_agents); // DEBUGGING

    }

    public static Integer getCostly() { return costly; }
    public static void setCostly(int cost) { costly = cost; }

}