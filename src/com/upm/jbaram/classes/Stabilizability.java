package com.upm.jbaram.classes;

import org.ejml.simple.SimpleMatrix;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class Stabilizability {

    public static final int BOUNDED_BY_DENSITY = 1;
    public static final int BOUNDED_BY_CLUSTER = 2;
    public static final int BOUNDED_BY_ARBORYC = 3;

    public static double DENSITY_THRESHOLD = 1;
    public static double CLUSTER_THRESHOLD = 1;
    public static String ARBORIC_THRESHOLD = "COMPLETE";

    private static SimpleMatrix delta;
    private static Random rng = new Random();

    public static boolean stabilizable(Map<Integer, Integer> currentVector) {
        boolean isBounded = false;
        List<Integer> team = currentVector.entrySet().stream().filter(v -> v.getValue()>0).map(Map.Entry::getKey).collect(Collectors.toList());
        switch (Knowledge.BOUNDING_FUNCTION) {
            case BOUNDED_BY_DENSITY:
                isBounded = densityFunction(team);
                break;
            case BOUNDED_BY_CLUSTER:
                isBounded = clusterFunction(team);
                break;
            case BOUNDED_BY_ARBORYC:
                isBounded = boundedFunction(team);
                break;
            default:
                isBounded = true;
        }
        //System.out.println(isBounded);
        return isBounded;
    }

    private static boolean densityFunction(List<Integer> team) {
        double density = team.size()>1 ? getTeamEdges(team)/(team.size()*(team.size()-1)) : 0;
        //System.out.println(Arrays.toString(team.toArray()) + " " + edges + " density: " + density);
        return density <= DENSITY_THRESHOLD;
    }

    private static boolean clusterFunction(List<Integer> team) {
        SimpleMatrix teamDelta = getTeamDelta(team);
        double clustering;
        if (teamDelta.numCols()>=3) {
            SimpleMatrix delta2 = crossprod(teamDelta, teamDelta);
            double triangles = crossprod(delta2, teamDelta).trace();
            for (int i = 0; i < team.size(); i++) delta2.set(i, i, 0);
            double triads = delta2.elementSum();
            //System.out.println("triangles: " + triangles + ", triads:" + triads + ", transitivity: " + triangles/triads);
            clustering = triangles/triads;
        } else clustering = 0;

        return !Double.isNaN(clustering) && clustering <= CLUSTER_THRESHOLD;
    }

    private static boolean boundedFunction(List<Integer> team) {
        int f_vertexes, edges = getTeamEdges(team).intValue(), vertexes = team.size();
        switch (ARBORIC_THRESHOLD) {
            case "1-TREE":
                f_vertexes = vertexes-1;
                break;
            case "2-TREE":
                f_vertexes = 2*(vertexes-1);
                break;
            case "3-TREE":
                f_vertexes = 3*(vertexes-1);
                break;
            case "COMPLETE":
            default:
                f_vertexes = binomial(vertexes, 2);
        }

        return f_vertexes >= edges;
    }

    static void genDelta(int agents){
        delta = new SimpleMatrix(agents, agents);

        for(int i = 0; i < agents; i++) {
            for(int j = i; j < agents; j++) {
                if(i == j) delta.set(i,j, 0);
                else{
                    int val = rng.nextInt(2);
                    delta.set(i,j, val);
                    delta.set(j,i, val);
                }
            }
        }
        //System.out.println(delta.toString());
    }

    public static String dumpTeamDelta(Map<Integer, Integer> currentVector) {
        List<Integer> team = currentVector.entrySet().stream().filter(v -> v.getValue()>0).map(Map.Entry::getKey).collect(Collectors.toList());
        SimpleMatrix teamDelta = getTeamDelta(team);

        String output = "delta:";
        for (int i=0; i<teamDelta.numRows(); i++) {
            for (int j=0; j<teamDelta.numCols(); j++)
                if (i>j && teamDelta.get(j,i)>0.0)
                    output += j + " " + i + ",";
        }
        //System.out.println(teamDelta.toString());
        return output;
    }

    private static SimpleMatrix getTeamDelta(List<Integer> team) {
        SimpleMatrix teamDelta = new SimpleMatrix(team.size(),team.size());
        int iAux = 0;
        for (Integer i : team) {
            int jAux = 0;
            for (Integer j : team) teamDelta.set(iAux,jAux++, delta.get(i-1,j-1));
            iAux++;
        }
        return teamDelta;
    }

    private static SimpleMatrix crossprod(SimpleMatrix matrix1, SimpleMatrix matrix2) {
        SimpleMatrix result = matrix1.copy();
        result.zero();
        for(int i = 0; i < matrix1.numRows(); i++) {         // rows from m1
            for(int j = 0; j < matrix2.numCols(); j++) {    // columns from m2
                for(int k = 0; k < matrix1.numCols(); k++) { // columns from m1
                    result.set(i, j, result.get(i, j) + matrix1.get(i, k)*matrix2.get(k, j));
                }
            }
        }
        return result;
    }

    private static Double getTeamEdges(List<Integer> team) {
        double edges = 0;
        for (Integer i : team) {
            for (Integer j : team) {
                if (!i.equals(j)) edges += delta.get(i-1,j-1);
            }
        }
        return edges;
    }

    public static int binomial(int n, int k) {
        if (k>n-k) k=n-k;
        int b=1;
        for (int i=1, m=n; i<=k; i++, m--)
            b=b*m/i;
        return b;
    }

}
