package com.upm.jbaram.classes;

import java.io.Serializable;
import java.util.Map;

public class Letter implements Serializable {

    private Integer from;
    private Integer[] data;
    private Map<Integer, Integer> map;
    private Map<Integer, Integer> token1;
    private Map<Integer, Integer> token2;
    private Map<Integer, Integer> token3;
    private int performative;

    public Letter() {
        this.from = -1;
        this.data = null;
        this.map = null;
        this.token1 = null;
        this.token2 = null;
    }

    public Letter(Integer id, Integer[] data) {
        this.from = id;
        this.data = data;
        this.map = null; //new TreeMap<>();
    }

    public Letter(Integer id, Integer[] data, Map<Integer, Integer> map, Map<Integer, Integer> token1, Map<Integer, Integer> token2, Map<Integer, Integer> token3) {
        this.from = id;
        this.data = data;
        this.map = map;
        this.token1 = token1;
        this.token2 = token2;
        this.token3 = token3;
    }

    public Integer getFrom() { return from; }
    public Integer[] getData() { return data; }
    public Map<Integer, Integer> getAffordability() { return map; }
    public Map<Integer, Integer> getTokenTeam() { return token1; }
    public Map<Integer, Integer> getTokenReco() { return token2; }
    public Map<Integer, Integer> getTokenDepr() { return token3; }
    public int getPerformative() { return performative; }

    public void setPerformative(int aclMessage) { performative = aclMessage; }

}
