package com.upm.jbaram.agents;

import com.upm.jbaram.agents.behaviours.dcop.sbbst.SBBSTBehaviour;
import org.javatuples.Quartet;

import java.util.List;

/**
 * @author Jose Maria Barambones
 * @version 0.1
 */
public class AgentTeam extends AgentBase {

    private static final long serialVersionUID = 6131999702134027814L;

    private Integer assignment = null;
    private Integer depCost;
    private Integer recCost;
    private List<Integer> alpha;

    @Override
    protected void setup() {
        super.setup(); //log("setup()");

        registerAgentDF(getClass().getName());

        depCost = ((Quartet<Integer, Integer, Integer, List<Integer>>)getArguments()[0]).getValue1();
        recCost = ((Quartet<Integer, Integer, Integer, List<Integer>>)getArguments()[0]).getValue2();
        alpha = ((Quartet<Integer, Integer, Integer, List<Integer>>)getArguments()[0]).getValue3();
        setID(Integer.valueOf(super.getLocalName()));

        this.addBehaviour(new SBBSTBehaviour(this));
        //this.addBehaviour(new SBaBBehaviour(this));

        //log("Setup finished. " + getID() + "-" + getOrder());
    }

    public Integer getAssignment() { return assignment; }
    public Integer getDepCost() { return depCost; }
    public Integer getRecCost() { return recCost; }
    public List<Integer> getAlpha() { return alpha; }

    public void setAssignment(Integer assignment) { this.assignment = assignment; }
}