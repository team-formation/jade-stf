package com.upm.jbaram.agents.behaviours;

import com.upm.jbaram.agents.AgentBase;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.ThreadedBehaviourFactory;

public class OrchestratorBehaviour extends BaseBehaviour {

    public OrchestratorBehaviour(AgentBase orchestrator) {
        super(orchestrator);

        ThreadedBehaviourFactory tbf = new ThreadedBehaviourFactory();
        ParallelBehaviour pb = new ParallelBehaviour();

        //pb.addSubBehaviour(tbf.wrap(handshakeRecvBehaviour()));
        //pb.addSubBehaviour(tbf.wrap(handshakeSendBehaviour()));

        addSubBehaviour(pb);

    }

}
