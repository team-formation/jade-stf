package com.upm.jbaram.agents.behaviours.dcop;

import com.upm.jbaram.agents.AgentBase;
import com.upm.jbaram.agents.behaviours.BaseBehaviour;

import com.upm.jbaram.classes.Letter;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.Map;

public class TFBehaviour extends BaseBehaviour {

    public TFBehaviour(AgentBase a) {
        super(a);
    }

    public void send (Letter info, Integer to) { this.send(info, to, ACLMessage.REQUEST); }

    public void send (Letter info, Integer to, int aclMessage) {
        ACLMessage msg = new ACLMessage(aclMessage);
        msg.addReceiver(new AID(to.toString(), AID.ISLOCALNAME));
        try {
            msg.setContentObject(info);
            getAgent().send(msg);
        } catch (IOException e) {
            e.printStackTrace();
            loge("Unable to send Letter from " + info.getFrom() + " to " + to);
        }
    }

    public Letter receive () { return receive(ACLMessage.REQUEST); }

    public Letter receive (int aclMessage) {

        ACLMessage msg = getAgent().receive(
                MessageTemplate.or(
                        MessageTemplate.MatchPerformative(ACLMessage.CANCEL),
                        MessageTemplate.or(
                                MessageTemplate.MatchPerformative(aclMessage),
                                MessageTemplate.MatchPerformative(ACLMessage.PROPOSE))
                ));
        if (msg != null) {
            try {
                int performative = msg.getPerformative();
                    Letter letter = (Letter)msg.getContentObject();
                    letter.setPerformative(performative);
                    return letter;
            } catch (UnreadableException e) {
                //e.printStackTrace();
                loge("Unable to deserialize Data from: " + msg.getSender().getName());
            }
        }
        return null;
    }

    public Letter letter(Integer... data) { return new Letter(ID, data); }

    @SafeVarargs
    public final Letter letter(Integer[] data, Map<Integer, Integer>... maps) {
        if (maps.length==4) return new Letter(ID, data, maps[0], maps[1], maps[2], maps[3]);
        else {
            loge("Bad letter configuration.");
            return letter();
        }
    }

}