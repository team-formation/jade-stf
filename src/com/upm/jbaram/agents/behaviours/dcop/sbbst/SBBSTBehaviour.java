package com.upm.jbaram.agents.behaviours.dcop.sbbst;

import com.upm.jbaram.Main;
import com.upm.jbaram.agents.AgentBase;
import com.upm.jbaram.agents.AgentTeam;
import com.upm.jbaram.agents.behaviours.dcop.TFBehaviour;
import com.upm.jbaram.classes.Letter;
import com.upm.jbaram.classes.Console;
import com.upm.jbaram.classes.Knowledge;
import com.upm.jbaram.classes.Stabilizability;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;

import jade.lang.acl.ACLMessage;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Quintet;

import java.util.*;
import java.util.stream.Collectors;

public class SBBSTBehaviour extends TFBehaviour {

    private static final String STATE_TEAM = "STATE_TEAM";
    private static final String STATE_ENDO = "STATE_END";

    private static final Integer EXHAUST = 2;
    private static final Integer NO_TEAM = 0;
    private static final Integer IN_TEAM = 1;

    private static final Integer SEARCH_MODE = 0;
    private static final Integer FINISH_MODE = 1;

    private int mode = SEARCH_MODE;

    private Integer DR; // Deploy Root agent
    private Integer DP; // Deploy Parent agent
    private Integer DC; // Deploy Child agent
    private Integer RR; // Recovery Root agent
    private Integer RP; // Recovery Parent agent
    private Integer RC; // Recovery Child agent

    private Integer K;
    private Integer cost;
    private Integer reco;
    private List<Integer> goal;
    private List<Integer> alpha;
    private Map<Integer, Integer> accomplish;
    private Map<Integer, Integer> teamToken;
    private Map<Integer, Integer> recoToken;
    private Map<Integer, Integer> deprToken;

    private int parentCostDeploy;
    private int parentCostRecovery;
    private Map<Integer, Integer> parentAffordability;
    private Map<Integer, Integer> parentRecoverability;

    private Integer inTeam;
    private Integer inReco;

    private static int allRecoverCombinations;
    private static List<Quintet<Integer, Map<Integer, Integer>, Map<Integer, Integer>, Map<Integer, Integer>, Map<Integer, Integer>>> solutions;

    public SBBSTBehaviour(AgentBase a) {
        super(a);

        clear();

        cost = ((AgentTeam)getAgent()).getDepCost();
        reco = ((AgentTeam)getAgent()).getRecCost();
        alpha = ((AgentTeam)getAgent()).getAlpha();

        accomplish = new HashMap<>();
        goal.forEach(v -> accomplish.put(v, alpha.contains(v) ? 1 : 0));

        FSMBehaviour fsm = new FSMBehaviour(getAgent()) {
            public int onEnd() {
                //log("DFSBehaviour completed.");
                getAgent().doSuspend();
                return super.onEnd();
            }
        };

        ID = ((AgentTeam)getAgent()).getID();
        inflateOrders();

        fsm.registerFirstState(new RecoveryBehaviour(), STATE_TEAM);
        fsm.registerLastState(new ReadyBehaviour(), STATE_ENDO);
        fsm.registerTransition(STATE_TEAM, STATE_TEAM, SEARCH_MODE);
        fsm.registerTransition(STATE_TEAM, STATE_ENDO, FINISH_MODE);

        addSubBehaviour(fsm);

        if (ID.equals(DR)) send(letter(), ID); // Initializes root domain.
    }

    private void inflateOrders() {

        List<Integer> agentDeployOrder = Knowledge.TF_agents.stream()
                .sorted(Comparator.comparing(Quartet::getValue1)) //.sorted(Comparator.comparing(v -> v.getValue3().size()))
                .map(Quartet::getValue0).collect(Collectors.toList());
        Collections.reverse(agentDeployOrder);

        int orderD = agentDeployOrder.indexOf(ID)+1;
        if (orderD<agentDeployOrder.size()) DC = agentDeployOrder.get(orderD);
        if (orderD == 1) DR = ID;
        else DP = agentDeployOrder.get(orderD-2);

        List<Integer> agentRecoverOrder = Knowledge.TF_agents.stream()
                .sorted(Comparator.comparing(Quartet::getValue2))
                .map(Quartet::getValue0).collect(Collectors.toList());
        Collections.reverse(agentRecoverOrder);

        int orderR = agentRecoverOrder.indexOf(ID)+1;
        if (orderR<agentRecoverOrder.size()) RC = agentRecoverOrder.get(orderR);
        if (orderR == 1) RR = ID;
        else RP = agentRecoverOrder.get(orderR-2);

        //Connect efficient and recovery paths
        if (DC==null) RR = agentRecoverOrder.get(0);
        if (ID.equals(RR)) RP = agentDeployOrder.get(agentDeployOrder.size()-1);

        //System.out.println("Agent: " + ID + " > " + orderD + "th; dc-" + cost + " > " + orderR + "th; rc-" + reco + " > " + "accomplish: " + Console.YELLOW + Arrays.toString(accomplish.entrySet().toArray()) + Console.RESET + " | " + DP + "<->" + DC + " | " + RP + "<->" + RC);

        if (ID.equals(RR)) ((AgentBase)this.getAgent()).log("init TF "+ Knowledge.TF_ITER);

    }

    private void clear() {
        mode = SEARCH_MODE;
        DR = null;
        DP = null;
        DC = null;
        RR = null;
        RP = null;
        RC = null;

        goal = List.of(Knowledge.TF_goal.toArray(Integer[]::new));
        alpha = new ArrayList<>();
        teamToken = new HashMap<>();
        recoToken = new HashMap<>();
        deprToken = new HashMap<>();

        parentCostDeploy = 0;
        parentCostRecovery = 0;
        parentAffordability = new HashMap<>();
        parentRecoverability = new HashMap<>();

        inTeam = EXHAUST;
        inReco = EXHAUST;

        allRecoverCombinations = 0;
        solutions = new ArrayList<>();
    }

    private class RecoveryBehaviour extends OneShotBehaviour {

        private List<Map<Integer, Integer>> rejectedTeams = new ArrayList<>();

        @Override
        public void action() {
            Letter sender = recv();
            Pair<Integer, Integer> output;
            switch (sender.getPerformative()) {
                case ACLMessage.REQUEST:
                    output = searchDeploymentTeam(sender);
                    send(letter(new Integer[]{parentCostDeploy + costDeploy()}, vectorDeploy(), teamToken, recoToken, deprToken), output.getValue0(), output.getValue1());
                    //System.out.println(ID + "->" + output.getValue0() + " [" + Console.PURPLE + inTeam + Console.RESET + "," + Console.BLUE + inReco + Console.RESET + ": " + Console.GREEN + Arrays.toString(vectorDeploy().entrySet().toArray()) + Console.RESET + Console.PURPLE + Arrays.toString(teamToken.entrySet().toArray()) + Console.RESET + "]");
                    break;
                case ACLMessage.PROPOSE:
                    output = searchRecoveryTeam(sender);
                    send(letter(new Integer[]{parentCostRecovery + costRecovery(), K}, vectorRecovery(), teamToken, recoToken, deprToken), output.getValue0(), output.getValue1());
                    //System.out.println(ID + "->" + output.getValue0() + " [" + Console.PURPLE + inTeam + Console.RESET + "," + Console.BLUE + inReco + Console.RESET + ": "+ Console.GREEN + Arrays.toString(vectorRecovery().entrySet().toArray()) + Console.RESET+ Console.YELLOW + Arrays.toString(deprToken.entrySet().toArray()) + Console.RESET+ Console.BLUE + Arrays.toString(recoToken.entrySet().toArray()) + Console.RESET + "]");
                    break;
                case ACLMessage.CANCEL:
                    mode = FINISH_MODE;
                    break;
                default:
                    loge(sender.getPerformative() + " UNEXPECTED FROM " + sender.getFrom());
                    mode = FINISH_MODE;
            }
        }

        private Pair<Integer,Integer> searchDeploymentTeam(Letter sender) {
            Integer from = sender.getFrom();
            Integer to = 0;
            int msg = ACLMessage.REQUEST;
            if (ID.equals(DR)) { // I am the deploy root
                if (from.equals(ID) || from.equals(DC)) { nextDomDeploy(); }
                if (!inTeam.equals(EXHAUST) && parentCostDeploy + costDeploy() <= Knowledge.getCostly()) {
                    goal.forEach(v -> parentAffordability.put(v, 0));
                    to = DC;
                } else {
                    to = ID;
                    msg = ACLMessage.CANCEL;
                }
            } else { // I am not the root
                if (from.equals(DP)) teamToken = sender.getTokenTeam();
                if (DC != null) {
                    switch (inTeam) {
                        case 0: // NO_TEAM
                            if (from.equals(DC) || from.equals(ID)) {
                                nextDomDeploy();
                                to = parentCostDeploy + costDeploy() <= Knowledge.getCostly() ? DC : DP;
                                if (to.equals(DP)) nextDomDeploy();
                            }
                            break;
                        case 1: // IN_TEAM
                            if (from.equals(DC)) {
                                nextDomDeploy();
                                to = DP;
                            }
                            break;
                        default: // EXHAUST
                            if (from.equals(DP)) {
                                nextDomDeploy();
                                parentAffordability = sender.getAffordability();
                                parentCostDeploy = sender.getData()[0];
                                to = parentCostDeploy + costDeploy() <= Knowledge.getCostly() ? DC : ID;
                            }
                    }
                } else { // I am a leaf
                    Pair<Integer, Integer> output = computeEfficiency(sender);
                    to = output.getValue0();
                    msg = output.getValue1();
                }
            }
            return new Pair<>(to, msg);
        }

        private Pair<Integer,Integer> computeEfficiency(Letter sender) {
            Integer from = sender.getFrom();
            Integer to = 0;
            boolean toRecoveryMode = false;
            switch (inTeam) {
                case 0: // NO_TEAM
                    if (from.equals(ID) || from.equals(RR)) {
                        nextDomDeploy();
                        to = (toRecoveryMode = computeMode(Main.log)) ? RR : DP;
                        if (to.equals(DP) && !toRecoveryMode) nextDomDeploy();
                    }
                    break;
                case 1: // IN_TEAM
                    if (from.equals(ID) || from.equals(RR)) {
                        nextDomDeploy();
                        to = DP;
                    }
                    break;
                default: // EXHAUST
                    if (from.equals(DP)) {
                        parentCostDeploy = sender.getData()[0];
                        parentAffordability = sender.getAffordability();
                        nextDomDeploy();
                        to = (toRecoveryMode = computeMode(Main.log)) ? RR : ID;
                    }
            }
            return new Pair<>(to, toRecoveryMode ? ACLMessage.PROPOSE : ACLMessage.REQUEST); //return new Pair<>(to, ACLMessage.REQUEST);
        }

        private boolean computeMode(boolean trace) {
            Map<Integer,Integer> currentVector = vectorDeploy();
            long efficiency = currentVector.values().stream().filter(v -> v>0).count();
            int costly = parentCostDeploy + costDeploy();
            if (efficiency==currentVector.size() && efficiency>0 && costly <= Knowledge.getCostly() && Stabilizability.stabilizable(teamToken)) {
                Knowledge.setTeamCache(Map.copyOf(teamToken));
                allRecoverCombinations = Stabilizability.binomial((int)Knowledge.getTeamCache().entrySet().stream().filter(v -> v.getValue().equals(1)).count(), Knowledge.getTfK());
                solutions.clear();
                deprToken.putAll(teamToken);
                if (trace) System.out.println(Console.CYAN + costly + "-efficient" + Console.RESET + ": "
                        + Console.GREEN + Arrays.toString(currentVector.entrySet().toArray()) + Console.RESET
                        + Console.PURPLE + Arrays.toString(teamToken.entrySet().toArray()) + Console.RESET);
                return true;
            }
            return false;
        }

        private Map<Integer, Integer> vectorDeploy() {
            if (inTeam.equals(IN_TEAM)) {
                Map<Integer, Integer> newAffordability = new HashMap<>();
                accomplish.forEach((t, a) -> newAffordability.put(t, parentAffordability.get(t)+a));
                return newAffordability;
            }
            return parentAffordability;
        }

        private void nextDomDeploy() { // Iterators are not serializable by default.
            if (inTeam.equals(EXHAUST)) {
                inTeam=NO_TEAM;
                teamToken.put(ID, inTeam);
            } else if (inTeam.equals(NO_TEAM)) {
                inTeam=IN_TEAM;
                teamToken.put(ID, inTeam);
            } else if (inTeam.equals(IN_TEAM)){
                inTeam=EXHAUST;
                teamToken.remove(ID);
            }
        }

        private Integer costDeploy() {
            if (inTeam.equals(EXHAUST) || inTeam.equals(NO_TEAM)) return 0;
            return cost;
        }

        private Pair<Integer,Integer> searchRecoveryTeam(Letter sender) {
            Integer from = sender.getFrom();
            Integer to = 0;
            int msg = ACLMessage.PROPOSE;
            if (ID.equals(RR)) { // I am the recovery root
                parentRecoverability = sender.getAffordability();
                if (from.equals(RP)) {
                    K = 0;
                    parentCostRecovery = sender.getData()[0];
                    deprToken = sender.getTokenDepr();
                }
                nextDomRecovery();
                if (!inReco.equals(EXHAUST)) to = RC;
                else {
                    to = RP;
                    msg = ACLMessage.REQUEST;
                    //nextDomRecovery();
                }
            } else { // I am not the root
                if (from.equals(RP)) {
                    recoToken = sender.getTokenReco();
                    deprToken = sender.getTokenDepr();
                }
                if (RC != null) {
                    switch (inReco) {
                        case 0: // NO_TEAM
                            if (from.equals(RC) || from.equals(ID)) {
                                nextDomRecovery();
                                if (!inReco.equals(EXHAUST)) {
                                    to = parentCostRecovery + costRecovery() <= Knowledge.getCostly() ? RC : RP;
                                    if (to.equals(RP)) nextDomRecovery();
                                } else to = RP;
                            }
                            break;
                        case 1: // IN_TEAM
                            if (from.equals(RC)) {
                                nextDomRecovery();
                                to = RP;
                            }
                            break;
                        default: // EXHAUST
                            if (from.equals(RP)) {
                                K = sender.getData()[1];
                                nextDomRecovery();
                                parentCostRecovery = sender.getData()[0];
                                parentRecoverability = sender.getAffordability();
                                to = parentCostRecovery + costRecovery() <= Knowledge.getCostly() ? RC : ID;
                            }
                    }
                } else { // I am a leaf
                    computeRecovery(sender);
                    to = RP;
                }
            }
            return new Pair<>(to, msg);
        }

        private void computeRecovery(Letter sender) {
            K = sender.getData()[1];
            parentCostRecovery = sender.getData()[0];
            parentRecoverability = sender.getAffordability();
            nextDomRecovery();
            while (!inReco.equals(EXHAUST)) {
                computeSolution(Main.log);
                nextDomRecovery();
            }
        }

        private void computeSolution(boolean trace) {
            Map<Integer,Integer> currentVector = vectorRecovery();
            long recovery = currentVector.values().stream().filter(v -> v>0).count();
            int costly = parentCostRecovery + costRecovery();
            if (K.equals(Knowledge.getTfK())) {
                if (!rejectedTeams.contains(recoToken) && recovery==currentVector.size() && costly <= Knowledge.getCostly() && Stabilizability.stabilizable(deprToken)) {
                    solutions.add(new Quintet<>(costly, currentVector, Map.copyOf(deprToken), Knowledge.getTeamCache(), Map.copyOf(recoToken)));
                    if (solutions.stream().filter(v -> v.getValue4().equals(recoToken)).count()==allRecoverCombinations) {
                        Knowledge.setCostly(costly);//*/Knowledge.setCostly(0); // Set to 'costly' for all solutions. Set to 0 just for find a solution
                        Knowledge.pushSolution(costly, Map.copyOf(recoToken));
                    }
                } else rejectedTeams.add(recoToken);
            }
            if (trace) System.out.println(Console.CYAN + "<" + costly + "," + K + ">-costly" + Console.RESET + ", "
                    + Console.GREEN + Arrays.toString(currentVector.entrySet().toArray()) + Console.RESET + ", D="
                    + Console.YELLOW + Arrays.toString(deprToken.entrySet().toArray()) + Console.RESET + ", T="
                    + Console.PURPLE + Arrays.toString(Knowledge.getTeamCache().entrySet().toArray()) + Console.RESET + ", R="
                    + Console.BLUE + Arrays.toString(recoToken.entrySet().toArray()) + Console.RESET);
        }

        private Map<Integer, Integer> vectorRecovery() {
            Map<Integer, Integer> newAffordability = new HashMap<>();
            if (inReco.equals(IN_TEAM)) {
                accomplish.forEach((t, a) -> newAffordability.put(t, parentRecoverability.get(t)+a));
            } else if (inTeam.equals(IN_TEAM) && deprToken.get(ID).equals(0)) {
                accomplish.forEach((t, a) -> newAffordability.put(t, parentRecoverability.get(t)-a));
            } else return parentRecoverability;
            return newAffordability;
        }

        private void nextDomRecovery() { // Iterators are not serializable by default.
            if (inTeam.equals(IN_TEAM)) nextDomDeprived();
            else nextDomRecoTeam();
        }

        private void nextDomDeprived() {
            if (inReco.equals(EXHAUST)) {
                inReco = NO_TEAM;
                recoToken.put(ID, inReco);
                if (K<Knowledge.getTfK()) {
                    deprToken.put(ID, 0);
                    K += 1;
                }
            } else if (inReco.equals(NO_TEAM) && deprToken.get(ID).equals(0)) {
                deprToken.put(ID, inTeam);
                K -= 1;
            } else if (inReco.equals(NO_TEAM)){
                inReco = EXHAUST;
                recoToken.remove(ID);
            }
            //System.out.println(ID + ": K is " + K);
        }

        private void nextDomRecoTeam() {
            if (inReco.equals(EXHAUST)) {
                inReco = NO_TEAM;
                recoToken.put(ID, inReco);
            } else if (inReco.equals(NO_TEAM) && goal.stream().anyMatch(v -> accomplish.get(v).equals(1))) {
                inReco = IN_TEAM;
                recoToken.put(ID, inReco);
            } else {
                inReco = EXHAUST;
                recoToken.remove(ID);
            }
        }

        private Integer costRecovery() {
            if ((inReco.equals(EXHAUST) || inReco.equals(NO_TEAM)) || inTeam.equals(IN_TEAM)) return 0;
            return reco;
        }

        private Letter recv() {
            Letter letter;
            while ((letter=receive())==null) block();
            return letter;
        }

        public int onEnd() { return mode; }

    }

    private class ReadyBehaviour extends OneShotBehaviour {

        @Override
        public void action() {
            if (ID.equals(DR)) {
                final String[] output = {""};
                output[0] += Console.CYAN_BOLD_BRIGHT + "TF " + Knowledge.TF_ITER + " SOLUTION" + Console.RESET;
                String thereSolution = !Knowledge.solution.isEmpty() ? " YES" : " NO";
                Knowledge.solution.forEach(v -> output[0] = output[0]
                    + " \n" + Console.CYAN_BOLD + "<" + v.getValue0() + "," + Knowledge.getTfK() + "," + v.getValue1() + ">-stabilizable" + Console.RESET + ":"
                    + " T=" + Console.PURPLE_BOLD + Arrays.toString(v.getValue2().entrySet().stream().sorted(Map.Entry.comparingByKey()).toArray()) + Console.RESET
                    + " R=" + Console.BLUE_BOLD + Arrays.toString(v.getValue3().entrySet().stream().sorted(Map.Entry.comparingByKey()).toArray()) + Console.RESET
                );
                System.out.println(output[0]);//*/
                Arrays.stream((((AgentTeam)getAgent()).getAgentsDF(getAgent().getClass().getName())))
                        .filter(v -> !v.getName().getLocalName().equals(ID.toString()))
                        .forEach(v -> send(letter(), Integer.valueOf(v.getName().getLocalName()), ACLMessage.CANCEL));
                //((AgentBase)getAgent()).shutdown();

                ((AgentBase)this.getAgent()).log("end TF "+ Knowledge.TF_ITER + thereSolution);

                //Knowledge.solution.forEach(v -> System.out.println(Stabilizability.dumpTeamDelta(v.getValue2())));

                send(null,0,ACLMessage.REQUEST_WHENEVER);
            }
            //((AgentTeam)getAgent()).deregisterAgentDF();
            this.getAgent().doDelete();
        }

    }

}
