package com.upm.jbaram.agents.behaviours;

import com.upm.jbaram.agents.AgentBase;

import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;


@SuppressWarnings("WeakerAccess")
public abstract class BaseBehaviour extends SequentialBehaviour {

    protected Integer ID;

    public BaseBehaviour(AgentBase a) {
        super(a);

        this.ID = ((AgentBase)this.getAgent()).getID();

        addSubBehaviour(new DelayBehaviour(a, 0));
    }

    public void log (String s) { ((AgentBase)this.getAgent()).log(s); }

    public void loge(String s) { ((AgentBase)this.getAgent()).loge(s); }

    protected class DelayBehaviour extends SimpleBehaviour {

        private long timeout, wakeupTime;
        private boolean finished = false;

        public DelayBehaviour(AgentBase a, long timeout) {
            super(a);
            this.timeout = timeout;
        }

        public void onStart() {
            wakeupTime = System.currentTimeMillis() + timeout;
        }

        public void action() {
            long dt = wakeupTime - System.currentTimeMillis();
            if (dt <= 0) finished = true; //log("timeout()");
            else block(dt); //log("block()");
        }

        public boolean done() { return finished; }
    }

}
