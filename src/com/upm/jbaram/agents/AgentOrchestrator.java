package com.upm.jbaram.agents;

import com.upm.jbaram.classes.Knowledge;
import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import org.javatuples.Quartet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;

import static java.lang.Thread.sleep;


/**
 * Agent Orchestrator.
 *
 * @author Jose Maria Barambones
 * @version 0.2
 */
public class AgentOrchestrator extends AgentBase {

    public static final String NICKNAME = "0";
    private static int ORCHESTRATOR_ID = 0;

    private static final long serialVersionUID = 6131999702134027851L;

    @Override
    protected void setup() {
        super.setup();

        //this.addBehaviour(new OrchestratorBehaviour(this));

        setID(ORCHESTRATOR_ID);

        generateTFProblem();

        registerAgentDF(AgentOrchestrator.class.getName());

        log("Setup finished.");
    }

    private void generateTFProblem() {
        if (getTFsFromCSV() /*Knowledge.loadTF3(1)*/)
            System.out.println("TF loaded...");
    }

    private boolean getTFsFromCSV() {

        jade.core.Runtime runtime = jade.core.Runtime.instance();
        Profile profile = new ProfileImpl(null, 1200, null);
        profile.setParameter(Profile.CONTAINER_NAME, "TFcontainer");
        profile.setParameter(Profile.MAIN_HOST, "localhost");
        ContainerController container = runtime.createAgentContainer(profile);

        try (BufferedReader csvReader = new BufferedReader(new FileReader(Knowledge.FILENAME))) {
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(";");
                Knowledge.genTFProblem(data);
                Knowledge.TF_ITER++;
                createAgents(container);
                //do {
                ACLMessage  msg = this.blockingReceive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST_WHENEVER)/*, 5000*/);
                if (msg==null) {
                    container.kill();
                    container = runtime.createAgentContainer(profile);
                }
                //break; // JUST ONE PROBLEM FOR DEBUGGING
            }
            this.shutdown();
        } catch (IOException | StaleProxyException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    private void createAgents(ContainerController container){
        doWait(100);
        Knowledge.TF_agents.stream().sorted(Comparator.comparing(Quartet::getValue1)).forEach(v -> {
            try {
                container.createNewAgent(v.getValue0().toString(), AgentTeam.class.getName(), new Object[] {v}).start();
            } catch (StaleProxyException e) {
                e.printStackTrace();
            }
        });
    }

}

