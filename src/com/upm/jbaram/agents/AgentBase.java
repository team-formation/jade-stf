package com.upm.jbaram.agents;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.ShutdownPlatform;
import jade.lang.acl.ACLMessage;

public abstract class AgentBase extends Agent {

    private int ID;

    public DFAgentDescription[] getAgentsDF(String type) {

        DFAgentDescription template=new DFAgentDescription();
        ServiceDescription templateSd=new ServiceDescription();
        templateSd.setType(type);
        template.addServices(templateSd);
        DFAgentDescription[] result = new DFAgentDescription[0];

        try {
            result = DFService.search(this, template);
        } catch (FIPAException e) {
            e.printStackTrace();
            loge("DFService.search does not work!!!");
        }

        return result;
    }

    public void registerAgentDF(String type) {

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(this.getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType(type);
        sd.setName(this.getLocalName());

        dfd.addServices(sd);

        try {
            DFAgentDescription[] results = DFService.search(this, dfd);
            if (results.length <= 0)
                DFService.register(this,dfd);
        } catch (FIPAException e) {
            loge("Unable to register.");
            this.doDelete();
        }

    }

    public void deregisterAgentDF() {
        try { DFService.deregister(this); }
        catch (FIPAException e){
            loge("Unable to deregister.");
            this.doDelete();
        }
    }

    @Override
    public void doDelete() {
        super.doDelete();
        //log(": Bye!!!");
    }

    public void log(String s) { System.out.println(System.currentTimeMillis() +  " " + getLocalName() + "(" + getClass().getSimpleName() + ") " + s + " "); }
    public void loge(String s) { System.err.println(System.currentTimeMillis() +  " " + getLocalName() + "(" + getClass().getSimpleName() + ") " + s + " "); }

    public int getID() { return ID; }
    public void setID(int ID) { this.ID = ID; }

    public void shutdown () {
        ACLMessage shutdownMessage = new ACLMessage(ACLMessage.REQUEST);
        Codec codec = new SLCodec();
        this.getContentManager().registerLanguage(codec);
        this.getContentManager().registerOntology(JADEManagementOntology.getInstance());
        shutdownMessage.addReceiver(this.getAMS());
        shutdownMessage.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
        shutdownMessage.setOntology(JADEManagementOntology.getInstance().getName());
        try {
            this.getContentManager().fillContent(shutdownMessage,new Action(this.getAID(), new ShutdownPlatform()));
            this.send(shutdownMessage);
        }
        catch (Exception e) {
            //loge(e.getMessage());
            System.exit(0);
        }
    }

}