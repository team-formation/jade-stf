package com.upm.jbaram;

import com.upm.jbaram.agents.AgentOrchestrator;

import com.upm.jbaram.classes.Knowledge;
import com.upm.jbaram.classes.Stabilizability;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.StaleProxyException;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * @author      Jose Maria Barambones <j.barambones@upm.es>
 * @version     1.0.2
 *
 * Main Class.
 */
public class Main {

    public static final boolean log = false;

    // Executed from Singletons.
    private static void loadBoot(){

        jade.wrapper.AgentContainer cc;

        // Get a hold on JADE runtime
        jade.core.Runtime rt = jade.core.Runtime.instance();

        // Exit the JVM when there are no more containers around
        rt.setCloseVM(true);
        System.out.println("Runtime created");

        // Create a default profile
        Profile profile = new ProfileImpl(null, 1200, null);
        //profile.setParameter("verbosity","5");
        System.out.println("Profile created");

        System.out.println("Launching a whole in-process platform..."+profile);
        cc = rt.createMainContainer(profile);

        try {
            // now set the default Profile to start a container
            ProfileImpl pContainer = new ProfileImpl(null, 1200, null);
            //System.out.println("Launching the agent container ..."+pContainer);

            rt.createAgentContainer(pContainer);
            //System.out.println("Launching the agent container after ..."+pContainer);

            System.out.println("Containers created");

            System.out.println("Launching the rma agent on the main container ...");
            cc.createNewAgent("rma","jade.tools.rma.rma", new Object[0]).start();
            cc.createNewAgent(AgentOrchestrator.NICKNAME, AgentOrchestrator.class.getName(), new Object[]{"0"}).start();

        } catch (StaleProxyException e) {
            System.err.println("Error during boot!!!");
            e.printStackTrace();
            System.exit(1);
        }

    }


    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("--------------\n--- SBB-ST ---\n--------------\n" +
                    "\n" +
                    "Use: tf_file k_value bound_function bound_value output_file\n" +
                    "\n" +
                    "Parameters:\n" +
                    "\ttf_file: csv semicolon file with the line format: agent_x_accomplish_tasks separated by spaces\n" +
                    "\tk_value: [1..n]\n" +
                    "\tbound_function: 1 (Density), 2 (Clustering), 3 (Arboricity)\n" +
                    "\tbound_value: [0..1] (for Density and Clustering), [1-TREE, 2-TREE, 3-TREE, COMPLETE] (for Arboricity)\n" +
                    "\n" +
                    "Examples:\n" +
                    "\tjava -jar StableTF.jar stf_5_4_demo.csv\n" +
                    "\tjava -jar StableTF.jar stf_5_4_demo.csv 1 1 0.5\n" +
                    "\tjava -jar StableTF.jar stf_5_4_demo.csv 1 3 1-TREE\n" +
                    "\tjava -jar StableTF.jar stf_5_4_demo.csv 1 3 COMPLETE output.txt"
            );
            System.exit(0);
        }

        System.out.println("Starting...");

        Knowledge.FILENAME = args[0];
        Knowledge.TF_K = args.length < 2 ? 1 : Integer.parseInt(args[1]);
        Knowledge.BOUNDING_FUNCTION = 0;

        loadBoot();

        System.out.println("MAS loaded...");

        if (args.length >= 3) {
            Knowledge.BOUNDING_FUNCTION = Integer.parseInt(args[2]);
            switch (Integer.parseInt(args[2])) {
                case Stabilizability.BOUNDED_BY_DENSITY:
                    Stabilizability.DENSITY_THRESHOLD = Double.parseDouble(args[3]);
                    break;
                case Stabilizability.BOUNDED_BY_CLUSTER:
                    Stabilizability.CLUSTER_THRESHOLD = Double.parseDouble(args[3]);
                    break;
                case Stabilizability.BOUNDED_BY_ARBORYC:
                    Stabilizability.ARBORIC_THRESHOLD = args[3];
                default:
                    Knowledge.BOUNDING_FUNCTION = 0;
            }
        }

        if (args.length == 5) {
            try {
                PrintStream fileOut = new PrintStream(args[4]);
                System.setOut(fileOut);
            } catch (FileNotFoundException e) {
                e.getMessage();
            }
        }

    }

}
