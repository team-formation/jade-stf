import csv
import random


def check_k(row):
    skills = "01234"
    for _char in skills:
        string = str(row)
        if string.count(_char) < 3:
            return False
    return True


def gen_stf(filename, agents, skills, goal=5, samples=100, trys=2000000):

    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file, delimiter=';')

        # writer.writerow(range(1,agents+1)) # Agents
        for _iter in range(0,samples):

            row = []
            found = False
            while not found and trys > 0:
                for _agent in range(0, agents):
                    skill = sorted(random.sample(range(0, goal), random.randint(1,skills)))
                    row.append(' '.join(map(str, skill)))
                found = check_k(row)
                if not found:
                    row = []
                trys -= 1
                if trys == 0:
                    print('exausted')

            writer.writerow(row)

#gen_stf('stf_5_3.csv', 5, 3)
#gen_stf('stf_8_4.csv', 8, 4)
#gen_stf('stf_8_6.csv', 8, 6)
#gen_stf('stf_10_3.csv', 10, 3)
#gen_stf('stf_10_4.csv', 10, 4)
#gen_stf('stf_10_6.csv', 10, 6)
#gen_stf('stf_10_8.csv', 10, 8)
#gen_stf('stf_12_4.csv', 12, 4)
#gen_stf('stf_12_6.csv', 12, 6)
#gen_stf('stf_12_8.csv', 12, 6)
#gen_stf('stf_15_3.csv', 15, 3)
#gen_stf('stf_20_4.csv', 20, 4)
#gen_stf('stf_20_6.csv', 20, 6)
#gen_stf('stf_20_8.csv', 20, 8)
#gen_stf('stf_15_5-12.csv', 15, 10)
#gen_stf('stf_20_6-16.csv', 20, 10)
#gen_stf('stf_25_4.csv', 25, 4)
#gen_stf('stf_50_5_10.csv', 50, 5, 10)
gen_stf('stf_50_5_50.csv', 50, 5, 50)
