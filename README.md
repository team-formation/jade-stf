# Synchronous Branch-and-Bound for Stabilizable Teams (SBB-ST)

### Structure

- data: examples of TF problems and solutions.
- frodo2: DCOP solving environment
- lib: libraries for MAS deployment and Data Structures
- src/com/upm/jbaram
    - agents: orchestator for MAS deployment and team agents for TF solving
        - behaviours: SBB algorithm for STF problem
    - classes: Aux classes and Stabilizability functions
    - Main class
### Use

Requires JAVA 12

##### Parameters

    tf_file k_value bound_function bound_value output_file

    tf_file: csv semicolon file with the line format: agent_x_accomplish_tasks separated by spaces
    k_value: [1..n]
    bound_function: 1 (Density), 2 (Clustering), 3 (Arboricity)
    bound_value: [0..1] (for Density and Clustering), [1-TREE, 2-TREE, 3-TREE, COMPLETE] (for Arboricity)

#### Examples
    java -jar StableTF.jar 
    java -jar StableTF.jar stf_5_4_demo.csv
    java -jar StableTF.jar stf_5_4_demo.csv 1 1 0.5
    java -jar StableTF.jar stf_5_4_demo.csv 2 2 0.05
    java -jar StableTF.jar stf_5_4_demo.csv 2 3 1-TREE
    java -jar StableTF.jar stf_5_4_demo.csv 3 3 COMPLETE output.txt
    
csv example (5 agents for 4 tasks)

    1 2 3 4;0 2 3 4;0 1 2 3;0 1 3 4;2 3
    0 2 3;1 2 3 4;0 2 3 4;0 1 2 3;1 4
    0 1;1 3 4;1 2 3;0 1 2 4;0 2 3 4
    0 1 2 4;1 2 3 4;0 1 3 4;0 1 2 3;0 1 3 4
    0 1 2 4;0 1 2 3;0 2 3 4;0 2 3 4;1 4